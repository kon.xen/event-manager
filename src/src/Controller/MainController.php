<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Event;
use App\Repository\EventRepository;
use App\Repository\VenueRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use App\Form\EventEntryType;

/**
 * @property EventRepository eventRepo
 * @property VenueRepository venueRepo
 */
class MainController extends AbstractController
{
    public function __construct(
        EventRepository $eventRepository,
        VenueRepository $venueRepository
    )
    {
        $this->eventRepo = $eventRepository;
        $this->venueRepo = $venueRepository;
    }

    /**
     * @Route("/", name="home")
     * @Template
     */
    public function index(): Response
    {
        return $this->render('index.html.twig' );
    }

    /**
     * @Route("/events", name="display_events")
     * @Template
     * @return Response
     */
    public function displayEvents(): Response
    {
        $events = $this->eventRepo->findAll();
        $venues = $this->venueRepo->findAll();
        return $this->render('display.html.twig', ['events' => $events, 'venues'=>$venues]);
    }

    /**
     * @Route("/create", name="create_event")
     * @param Request $request
     * @return Response
     */
    public function createEvent( Request $request ): Response
    {
        $event = new Event();

        $form = $this->createForm(EventEntryType::class, $event);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $event = $form->getData();
            $venue = $event->getVenue();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($event);
            $entityManager->persist($venue);
            $entityManager->flush();

            return $this->redirectToRoute('create_event');
        }

        return $this->render('logger.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
