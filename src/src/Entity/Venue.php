<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VenueRepository")
 */
class Venue
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $address;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Event", mappedBy="venue")
     */
    private $eventId;

    public function __construct()
    {
        $this->eventId = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEventId(): Collection
    {
        return $this->eventId;
    }

    public function addEventId(Event $eventId): self
    {
        if (!$this->eventId->contains($eventId)) {
            $this->eventId[] = $eventId;
            $eventId->setVenue($this);
        }

        return $this;
    }

    public function removeEventId(Event $eventId): self
    {
        if ($this->eventId->contains($eventId)) {
            $this->eventId->removeElement($eventId);
            // set the owning side to null (unless already changed)
            if ($eventId->getVenue() === $this) {
                $eventId->setVenue(null);
            }
        }

        return $this;
    }
}
