# Event Manager
Coding by Kon.Xen

Symfony 5 docker Dev. Env. by Pham.

## To run the docker 
Make sure there are no other containers running.
```docker stats```
if there are, please stop them, or change this docker configuration to avoid port clashing etc.
navigate in to the docker directory ```cd docker``` and run;

1st time:
```
docker-compose build
docker-compose up
```

To use the app, point you browser to:  
http://localhost:8000/

And start entering events.

Have fun :)
## Commands (docker)
To execute commands on the service
```
 docker-compose exec php-fpm sh
```
To run Composer(or enythig else) from local

```
docker-compose run php-fpm composer 
```
## Trubleshooting
This is the first docker Dev Env. I ever "set up".
If you have any problems please call me or drop me a line.

